package com.gitlab.josercl;

import com.gitlab.josercl.generator.base.IGenerator;
import com.gitlab.josercl.generator.base.impl.application.ApplicationGenerator;
import com.gitlab.josercl.generator.base.impl.domain.DomainGenerator;
import com.gitlab.josercl.generator.base.impl.infrastructure.InfraGenerator;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Mojo(name = "generateCrud", aggregator = true)
public class GenerateCrudMojo extends AbstractMojo {

    @Parameter(defaultValue = "${project}", required = true, readonly = true)
    private MavenProject project;

    @Parameter(property = "entities", readonly = true, required = true)
    private String entities;

    @Parameter(property = "only", readonly = true)
    private String only;

    @Parameter(property = "basePackage", readonly = true)
    private String basePackage;

    public void execute() {
        String projectPath = project.getBasedir().getAbsolutePath();

        String[] entityList = entities.split(",");

        String realPackage = Optional.ofNullable(basePackage).orElse(project.getGroupId());

        List<IGenerator> generatorsToUse = new ArrayList<>();

        if (only == null) {
            generatorsToUse.add(new DomainGenerator(projectPath));
            generatorsToUse.add(new InfraGenerator(projectPath));
            generatorsToUse.add(new ApplicationGenerator(projectPath));
        } else {
            String[] onlies = only.split(",");

            for (String s : onlies) {
                switch (s) {
                    case "domain" -> generatorsToUse.add(new DomainGenerator(projectPath));
                    case "infra", "infrastructure" -> generatorsToUse.add(new InfraGenerator(projectPath));
                    case "application", "app" -> generatorsToUse.add(new ApplicationGenerator(projectPath));
                }
            }
        }

        for (String entity : entityList) {
            for (IGenerator iGenerator : generatorsToUse) {
                try {
                    iGenerator.generate(entity, realPackage);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }
}
